import { createRouter, createWebHistory } from 'vue-router'

import sliderComp from "@/components/sliderComp";

const routes = [
  {
    path: '/',
    name: 'home',
    component: sliderComp
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
