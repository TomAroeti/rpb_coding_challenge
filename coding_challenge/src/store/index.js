import { createStore } from 'vuex'
import axios from 'axios'





export default createStore({
  state: {
    animals: []
  },

  mutations: {

    setAnimals(state, animals){
      state.animals = animals
    }
  },
  actions: {

    async getAnimals(context){
        const response = await axios.get('https://zoo-animal-api.herokuapp.com/animals/rand/5')
      context.commit('setAnimals', response.data)
      console.log(response)
    }

  },

})
